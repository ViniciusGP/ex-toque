from django.views.generic import ListView, UpdateView
from stock.models import Product
from django.shortcuts import redirect

class ProductListView(ListView):
    model = Product

class OutOfStockListView (ListView):
    model = Product
    
    def get_queryset(self):
        products = super().get_queryset()
        return products.filter(amount=0)
    
class AddStockView(UpdateView):
    model = Product
    
    def post(self, request, *args, **kwargs):
        product = self.get_object()
        amount = self.request.POST['amount']
        product.amount += int(amount)
        product.save()
        return redirect('product-list')

class RemoveStockView(UpdateView):
    model = Product
    
    def post(self, request, *args, **kwargs):
        product = self.get_object()
        amount = self.request.POST['amount']
        product.amount -= int(amount)
        product.save()
        return redirect('product-list')
        
